// src/Button/Button.js
import React from 'react';

const Button = ({ primary, children, onClick }) => {
  const buttonStyle = {
    padding: '10px 20px',
    fontSize: '16px',
    backgroundColor: primary ? 'blue' : 'gray',
    color: 'white',
    borderRadius: '5px',
    cursor: 'pointer',
  };

  return (
    <button style={buttonStyle} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
