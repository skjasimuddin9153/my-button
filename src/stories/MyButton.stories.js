// Button.stories.js
import React from 'react';
import Button from '../Button';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    variant: {
      options: ['primary', 'secondary'],
      control: { type: 'radio' },
    },
  },
  
};


export const Primary = () => (
  <Button primary onClick={action('Primary button clicked')}>
    Primary Button
  </Button>
);

export const Secondary = () => (
  <Button onClick={action('Secondary button clicked')}>
    Secondary Button
  </Button>
);
