"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _react = _interopRequireDefault(require("react"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
// src/Button/Button.js

var Button = function Button(_ref) {
  var primary = _ref.primary,
    children = _ref.children,
    onClick = _ref.onClick;
  var buttonStyle = {
    padding: '10px 20px',
    fontSize: '16px',
    backgroundColor: primary ? 'blue' : 'gray',
    color: 'white',
    borderRadius: '5px',
    cursor: 'pointer'
  };
  return /*#__PURE__*/_react["default"].createElement("button", {
    style: buttonStyle,
    onClick: onClick
  }, children);
};
var _default = exports["default"] = Button;