"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.Secondary = exports.Primary = void 0;
var _react = _interopRequireDefault(require("react"));
var _Button = _interopRequireDefault(require("../Button"));
var _addonActions = require("@storybook/addon-actions");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
// Button.stories.js
var _default = exports["default"] = {
  title: 'Button',
  component: _Button["default"],
  argTypes: {
    variant: {
      options: ['primary', 'secondary'],
      control: {
        type: 'radio'
      }
    }
  }
};
var Primary = exports.Primary = function Primary() {
  return /*#__PURE__*/_react["default"].createElement(_Button["default"], {
    primary: true,
    onClick: (0, _addonActions.action)('Primary button clicked')
  }, "Primary Button");
};
var Secondary = exports.Secondary = function Secondary() {
  return /*#__PURE__*/_react["default"].createElement(_Button["default"], {
    onClick: (0, _addonActions.action)('Secondary button clicked')
  }, "Secondary Button");
};